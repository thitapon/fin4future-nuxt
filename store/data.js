import axios from 'axios'

// States
export const state = () => ({
  // Chart data
  chartInvestmentMapData: [],
  chartInvestmentMapStatus: false, // False means still waiting for data

  // Mutual Fund List data
  mutualfundData: [],
  mutualfundDataStatus: false, // False means still waiting for data
})

// Getters
export const getters = {
  // Investment Map
  getInvestmentMapDataStatus: state => {
    return state.chartInvestmentMapStatus
  },
  getInvestmentMapData: state => {
    return state.chartInvestmentMapData
  },

  // Mutual fund list
  getMutualfundData: state => {
    return state.mutualfundData
  },
  getMutualfundDataStatus: state => {
    return state.mutualfundDataStatus
  },
}

// Mutations
export const mutations = {
  // Investment Map
  setInvestmentMapData(state, payload) {
    state.chartInvestmentMapData = payload.data
  },
  setInvestmentMapStatus(state, payload) {
    state.chartInvestmentMapStatus = payload.status
  },

  // Mutual Fund List Data
  setMutualfundData(state, payload) {
    state.mutualfundData = payload.data
  },
  setMutualfundDataStatus(state, payload) {
    state.mutualfundDataStatus = payload.status
  },
}

// Actions
export const actions = {
  retrieveInvestmentChartData({ state, commit }) {
    return new Promise((resolve, reject) => {
      // Set status of chart to loading data
      commit('setInvestmentMapStatus', { status: false })

      if (
        !Array.isArray(state.chartInvestmentMapData) ||
        !state.chartInvestmentMapData.length
      ) {
        // If there are no data
        // console.log('retrieving data')
        axios
          .get('http://fedr.utcc.ac.th/api/imap.php?table=invst_map_1yx')
          .then(res => {
            // console.log(res.data)
            const data = []

            for (let i = 0; i < res.data.length; i++) {
              data.push({
                type: res.data[i].type,
                name: res.data[i].name,
                riskOrder: res.data[i].risk,
                returnOrder: res.data[i].return,
                colorOrder: res.data[i].se,
                risk: res.data[i].risk_value,
                return: res.data[i].mean_value,
                id: res.data[i].id,
              })
            }
            // console.log(data)

            commit('setInvestmentMapData', { data: data })
            commit('setInvestmentMapStatus', { status: true })
            // console.log(state.chartInvestmentMapData)
            // console.log('exit')

            resolve()
          })
          .catch(err => {
            console.log('Error in retrieveInvestmentChartData Action.')
            reject(err)
          })
      } else {
        // Already retrieved data
        commit('setInvestmentMapStatus', { status: true })
        resolve()
      }
    })
  },

  retrieveMutualfundData({ state, commit }) {
    return new Promise((resolve, reject) => {
      // Set status of chart to loading data
      commit('setMutualfundDataStatus', { status: false })

      if (
        !Array.isArray(state.mutualfundData) ||
        !state.mutualfundData.length
      ) {
        // If there are no data
        axios
          .get('http://fedr.utcc.ac.th/api/fund-all')
          .then(res => {
            // console.log(res.data)
            const data = res.data.map(d => {
              return {
                fund_id: d.id,
                fund_init_name: d.name,
                fund_name_th: d.fund_name_th,
                fund_comp_name: d.company_en,
                fund_div_pol: d.dividend,
                fund_inv_pol: d.invest_flag,
                fund_pol: d.policy_desc,
                fund_spec_code: d.spec_code,
                fund_name_en: d.fund_name_en, // New fields starts here
                fund_comp_id: d.company_id,
                fund_comp_name_th: d.company_th,
              }
            })
            // console.log(data)

            commit('setMutualfundData', { data: data })
            commit('setMutualfundDataStatus', { status: true })
            // console.log(state.mutualfundData)
            resolve()
          })
          .catch(err => {
            console.log('Error in retrieveMutualfundData Action.')
            reject(err)
          })
      } else {
        // Already retrieved data
        commit('setMutualfundDataStatus', { status: true })
        resolve()
      }
    })
  },
}
