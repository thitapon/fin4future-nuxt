// States
export const state = () => ({
  windowSize: {
    windowHeight: 0,
    windowWidth: 0,
  },
  drawer: null,
})

// Getters
export const getters = {
  HomeTopCardHeight: state => multiplier => {
    return state.windowSize.windowHeight * multiplier
  },
  getDrawerStatus: state => {
    return state.drawer
  },
}

// Mutations
export const mutations = {
  setWindowSize(state, payload) {
    // The top toolbar height is currently 54
    state.windowSize.windowHeight = payload.windowHeight - 54
    state.windowSize.windowWidth = payload.windowWidth
  },
  switchDrawerStatus(state) {
    state.drawer = !state.drawer
  },
}

// Actions
export const actions = {
  onResize({ commit }) {
    const windowSize = {
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    }
    commit('setWindowSize', windowSize)
  },
}
